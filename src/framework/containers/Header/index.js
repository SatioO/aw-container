import "./header.scss";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link, withRouter } from "react-router-dom";
import { Logo, Login, ZipCode, Search, BrandStripe } from "../../shared";

const dummy = () => <h2 className="aw-button">Coming Soon</h2>;

class Header extends Component {
	state = {
		listConfig: {
			1: {
				id: 1,
				text: "",
				component: ZipCode,
				isActive: false,
				className: "fa fa-map-marker hiderest"
			},
			2: {
				id: 2,
				text: "",
				component: dummy,
				isActive: false,
				className: "fa fa-bell hiderest"
			},
			3: {
				id: 3,
				text: "",
				component: Search,
				isActive: false,
				className: "fa fa-search hiderest"
			},
			4: {
				id: 4,
				text: "Log In",
				component: () => <Login onSave={this.onSave} />,
				isActive: false,
				className: "fa fa-user"
			}
		}
	};

	onSave = e => {
		setTimeout(_ => {
			this.setState({
				listConfig: {
					...this.state.listConfig,
					[4]: {
						...this.state.listConfig[4],
						isActive: false,
						text: e.data.user.username
					}
				}
			});
		}, 0);
	};
	constructor(props) {
		super(props);
		this.initialState = { ...this.state };
	}

	toggleTabs = item => {
		this.setState({ ...this.initialState });
		if (item.isActive) {
			setTimeout(_ => {
				this.setState({
					listConfig: {
						...this.state.listConfig,
						[item.id]: {
							...item,
							isActive: false
						}
					}
				});
			}, 0);
		} else {
			setTimeout(_ => {
				this.setState({
					listConfig: {
						...this.state.listConfig,
						[item.id]: {
							...item,
							isActive: true
						}
					}
				});
			}, 0);
		}
	};

	render() {
		const { listConfig } = this.state;

		return (
			<div
				className={
					this.props.isFixedWidth ? "fixed header-wrapper" : "header-wrapper"
				}
			>
				<div className="header shadow-1 item-gap">
					<BrandStripe height={"4px"} />
					<Logo src="assets/logos/am_water_logo_HOR_color.png" />
					{this.path}
					<ul
						className={
							this.props.location.pathname !== "/"
								? "list aw-tabs header-options left-list hiderest"
								: "list aw-tabs header-options left-list showrest"
						}
					>
						<li>
							<Link to={"/myprofile"}>Profile</Link>
						</li>
						<li>
							<a>Lorem</a>
						</li>
						<li>
							<a>Lorem</a>
						</li>
						<li>
							<a>Lorem</a>
						</li>
					</ul>
					<ul className="list aw-tabs header-options right-list">
						{Object.keys(listConfig).map((item, index) => (
							<li
								id={listConfig[item].id}
								key={index}
								onClick={_ => this.toggleTabs(listConfig[item])}
								className={this.props.location.pathname !== '/' ? ' hiderest': 'showrest' }
							>
								<a>
									<i className={listConfig[item].className} />
									<span>{listConfig[item].text}</span>
									{listConfig[item].isActive}
								</a>
							</li>
						))}
					</ul>
					<div className="aw-tabs-blocks">
						{Object.keys(listConfig).map((item, index) => {
							const Itemcomponent = listConfig[item].component;
							return (
								<div
									id={`block${listConfig[item].id}`}
									key={index}
									className={listConfig[item].isActive ? "active" : null}
								>
									<Itemcomponent />
								</div>
							);
						})}
					</div>
				</div>
			</div>
		);
	}
}

Header.propTypes = {
	history: PropTypes.any,
	isFixedWidth: PropTypes.bool
};

export default withRouter(Header);
