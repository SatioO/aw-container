import { NOT_LOADED, defaultConfig, toName } from "./helpers";

const apps = [];

export const getAppNames = () => apps.map(toName);

export const registerChildApplication = (
	name = "",
	routes,
	config = defaultConfig
) => {
	if (typeof name !== "string" || name.length === 0)
		throw new Error(`The first argument must be a non-empty string 'appName'`);

	if (getAppNames().indexOf(name) !== -1)
		throw new Error(`There is already an app declared with name ${name}`);

	apps.push({ name: name, status: NOT_LOADED, routes: routes, config: config });
};

export const start = done => done(apps);
