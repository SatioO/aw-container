import React from "react";

export default [
	{
		path: "/analytics",
		exact: true,
		component: () => <h1>Analytics Page</h1>
	},
	{
		path: "/analytics/first",
		component: () => <h1>Analytics First Nested Page</h1>
	}
];
