import React from "react";
import "./index.scss";
import PropTypes from "prop-types";

const BrandStripe = props => (
	
	<span className="brand-color-stripe" style={{ height: props.height }}>
		<span />
		<span />
		<span />
		<span />
		<span />
		<span />
	</span>
);

BrandStripe.propTypes = {
	height: PropTypes.string
};

export default BrandStripe;
