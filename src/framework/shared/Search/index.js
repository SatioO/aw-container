import React from "react";
import Speech from "./Speech";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

class Search extends React.Component {
	_handleKeyPress = e => {
		if (e.key === "Enter" && e.target.value.length > 0) {
			this.props.history.push({
				pathname: "/searchresult",
				search: `query=${e.target.value}`
			});
		}
	};

	_handlevoice = () => {
		new Speech().start(this.getText);
	};

	getText = text => {
		if (!!text) {
			document.getElementById("voice").value = text;
			setTimeout(() => {
				this.props.history.push({
					pathname: "/searchresult",
					search: `query=${text}`
				});
			}, 2000);
		}
	};

	render() {
		return (
			<div className="aw-button search-wrapper">
				<div className="search-input-wrapper">
					<i className="fa fa-search" />
					<input
						type="search"
						id="voice"
						placeholder="SEARCH"
						onKeyPress={this._handleKeyPress}
					/>
					<i className="fa fa-microphone" onClick={this._handlevoice} />
				</div>
			</div>
		);
	}
}

Search.propTypes = {
	history: PropTypes.any
};

export default withRouter(Search);
