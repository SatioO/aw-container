import React from "react";
import { withRouter } from "react-router-dom";
import "./index.scss";
export default (CardComponent, DetailedComponent) =>
	withRouter(
		class extends React.Component {
			state = {
				error: false,
				card: {
					classNames: "is-4"
				},
				expanded: false,
				collpased: true,
				params: {},
				events: {
					expand: this.expand.bind(this),
					collapse: this.collapse.bind(this)
				}
			};

			expand(params) {
				if (!params && typeof params !== "object") {
					params = {};
				}

				this.setState({
					expanded: true,
					collapsed: false,
					params: { ...this.state.params, params }
				});
			}

			collapse() {
				this.setState({
					expanded: false,
					collapsed: true
				});
			}

			componentWillMount() {
				const { card } = this.state;
				const { config } = this.props;

				if (!!config) {
					this.setState({
						card: {
							...card,
							classNames: config.classNames ? config.classNames : "is-4"
						}
					});
				}
			}

			componentDidCatch(error, info) {
				this.setState({ error: true });
				this.logError(error, info);
			}

			get value() {
				return this.config;
			}

			logError = (error, info) => console.log(error, info);

			render() {
				const { error, expanded, events, params } = this.state;

				return (
					<div className={`column ${this.state.card.classNames}`}>
						{!error ? (
							<div className="item-gap aw-card">
								<CardComponent {...this.props} {...this.state.events} />
							</div>
						) : (
							<p>Something went wrong ..!</p>
						)}

						<div className={"expanded-view " + (expanded ? "open" : "")}>
							{expanded ? (
								<DetailedComponent {...this.props} {...events} {...params} />
							) : null}
						</div>
						<div
							className={"expanded-view-backdrop " + (expanded ? "open" : "")}
						/>
					</div>
				);
			}
		}
	);
