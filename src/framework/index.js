export { getAppNames, registerChildApplication, start } from "./register";
export { default as HOC } from "./HOC";
export { default as config } from "./configurations";
export * from "./containers";
