import "./index.scss";
import "font-awesome/css/font-awesome.min.css";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { registerChildApplication } from "framework";

registerChildApplication("dashboard", () => import("./dashboard"));

registerChildApplication(
	"content-application",
	() => import("./content-team"),
	() => import("./content-team/config")
);

registerChildApplication("billing-payments", () => import("./billing-payment"));

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
