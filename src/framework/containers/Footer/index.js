import "./footer.scss";
import React from "react";
import PropTypes from "prop-types";
import { Logo, BrandStripe } from "../../shared";

const Footer = ({ data , isFixedWidth}) => (
	<div className={"footer-container " +  (isFixedWidth ? "fixed" : "")}>
		<div className="footer-block">
			<div className="">
				<div className="footer-wrapper">
					<BrandStripe height={"9px"} />
					<div className="section">
						<h5>My Account</h5>
						<br />
						<p>{data.my_account.subtitle}</p>
					</div>
					<div className="section">
						<h5>{data.contact_us.title}</h5>
						<h2>
							<i className="fa fa-whatsapp" /> <span>1.888.237.1333</span>
						</h2>
						<p>{data.contact_us.subtitle}</p>
						<br />
					</div>
					<div className="section">
						<h5 className="titles">Careers</h5>
						<br />
						<p>Join our team at American Water</p>
						<p>
							<a
								href="https://career4.successfactors.com/career?company=amwater"
								target="_blank"
								rel="noopener noreferrer"
							>
								Search opening
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div className="footer-block-white">
				<div className="footer-wrapper-white">
					<div className="section">
						<Logo src="assets/logos/am_water_logo_HOR_color.png" />
					</div>
					<div className="section" style={{ flexDirection: "column" }}>
						<p>
							© 2017 American Water. "American Water" and the star logo are the
							registered trademark of American Water Works Company, Inc. All
							rights reserved.
						</p>
						<h5>
							<a href="https://amwater.com/corp/privacy-policy">
								Privacy Policy
							</a>&nbsp;
							| &nbsp;<a href="https://amwater.com/corp/terms-of-use">Terms of Use</a>
						</h5>
					</div>
					<div className="section">
						<ul className="social-links">
							{data.social.links.map((item, i) => (
								<li key={i}>
									<a href={item.link}>
										<i className={item.icons} />
									</a>
								</li>
							))}
						</ul>
					</div>
				</div>
			</div>
	</div>
);

const Type = PropTypes.shape({
	title: PropTypes.string,
	subtitle: PropTypes.string,
	icon: PropTypes.string,
	links: PropTypes.array
});

Footer.propTypes = {
	data: PropTypes.shape({
		about_us: Type,
		contact_us: Type,
		social: Type,
		my_account: Type
	}),
	isFixedWidth: PropTypes.bool
};

export default Footer;
