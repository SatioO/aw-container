import "./index.scss";
import React from "react";
import { Route } from "react-router-dom";
import { config } from "../../index";
import { Header, Footer, SideBar, AlertBar } from "../index";
import { Layout } from "antd";
import data from "../Footer/data.json";
const { Content } = Layout;
export default ({ component: Component, ...rest }) => {
	return (
		<Route
			{...rest}
			render={matchProps => (
				<div className="defaultLayout">
					{!config.layout.skipHeader.has(rest.path) ? (
						<Header isFixedWidth={true} />
					) : null}
					{!config.layout.skipBanner.has(rest.path) ? (
						<div className="banner-container" />
					) : null}
					<div
						className={
							!config.layout.skipBanner.has(rest.path) !== -1
								? "dashboard-wrapper"
								: "dashboard-wrapper push-up-85"
						}
					>
						<Layout className="dashboard-container">
							{!config.layout.skipSideBar.has(rest.path) ? <SideBar /> : null}

							<Content>
								{!config.layout.skipAlert.has(rest.path) ? <AlertBar /> : null}
								<Component {...matchProps} />
							</Content>
						</Layout>
					</div>
					{!config.layout.skipSideBar.has(rest.path) ? (
						<Footer isFixedWidth={true} data={data} height={9} />
					) : null}
				</div>
			)}
		/>
	);
};
