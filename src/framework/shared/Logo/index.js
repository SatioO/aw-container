import "./logo.scss";
import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";
const Logo = ({ src }) => (
    <div className="logo">
        
		<Link to={"/dashboard"}>
            <img src={src} alt={src} />
            </Link>
        
    </div>
);

Logo.propTypes = {
    src: PropTypes.string.isRequired
};

export default Logo;
