import React, { Component } from "react";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import { config, start, DefaultLayout } from "framework";

class App extends Component {
	state = {
		routes: []
	};

	componentDidMount() {
		start(apps => {
			apps.forEach((app, index) => {
				app.routes().then(route => {
					app.config().then(values => {
						this.handleConfig(values);

						this.setState({
							routes: [...this.state.routes, ...route.default]
						});
					});
				});
			});
		});
	}

	handleConfig(values) {
		Object.keys(config.layout).forEach(key => {
			if (values.default[key]) {
				config.layout[key].add(...values.default[key]);
			}
		});
	}

	render() {
		const { routes } = this.state;

		return (
			<Router>
				<Switch>
					{routes.map(({ path, exact, component }, key) => (
						<DefaultLayout
							exact={exact}
							path={path}
							component={component}
							key={key}
						/>
					))}
				</Switch>
			</Router>
		);
	}
}

export default App;
