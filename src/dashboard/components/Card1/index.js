import React from "react";
import HOC from "HOC";
import { Card } from "antd";

const gridStyle = {
	width: "100%",
	textAlign: "center"
};

const Card1 = HOC(
	({ expand }) => (
		<Card title="Card title" hoverable="true">
			<p>Card content</p>
			<p>Card content</p>
			<p>Card content</p>
			<button onClick={() => expand({ message: "{data: []}" })}>Expand</button>
		</Card>
	),
	({ params, collapse }) => (
		<Card>
			<Card.Grid style={gridStyle}>
				<h1>{params.message}</h1>
				<button onClick={() => collapse()}>Collpase</button>
			</Card.Grid>
		</Card>
	)
);

export default Card1;
