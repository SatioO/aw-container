import React from "react";
import { Link } from "react-router-dom";

export default () => (
	<div>
		<h1>Home Page</h1>
		<Link to={"/dashboard"}>
			<button>Go To Dashboard</button>
		</Link>
	</div>
);
