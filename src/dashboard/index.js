import Dashboard from "./containers";

export default [
	{
		path: "/dashboard",
		exact: true,
		component: Dashboard
	}
];
