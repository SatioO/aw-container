import React from "react";

export default [
	{
		path: "/billing",
		exact: true,
		component: () => <h1>Billing And Payments Page</h1>
	},
	{
		path: "/billing/first",
		component: () => <h1>Billing And Payments First Nested Page</h1>
	}
];
