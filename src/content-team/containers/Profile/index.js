import React from "react";
import { Link } from "react-router-dom";

export default () => (
	<div>
		<h1>Profile Page</h1>
		<Link to={"/"}>
			<button>Back</button>
		</Link>
	</div>
);
