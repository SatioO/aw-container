// import { Modal, Button } from "antd";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal } from "antd";
import "./modal.scss";

class CPModal extends Component {
	closeModal = () => {
		this.props.clk(this.props.show);
	};

	render() {
		return (
			<div>
				<Modal
					title={null}
					wrapClassName="vertical-center-modal"
					visible={this.props.show}
					onCancel={this.closeModal}
					footer={null}
				>
					<p>some contents...</p>
					<p>some contents...</p>
					<p>some contents...</p>
				</Modal>
			</div>
		);
	}
}

CPModal.propTypes = {
	show: PropTypes.any,
	clk: PropTypes.func
};

export default CPModal;
