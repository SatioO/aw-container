export default {
	skipHeader: ["/"],
	skipSideBar: [],
	skipBanner: ["/"],
	skipFooter: ["/"],
	skipAlert: []
};
