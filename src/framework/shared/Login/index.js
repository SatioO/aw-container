// import "./login.scss";
// import React, { Component } from "react";
// import PropTypes from "prop-types";
// import { connect } from "react-redux";
// import { checkAuth } from "store/actions/auth";
// import { Route, Redirect } from "react-router-dom";
// import _ from "underscore";

// class Login extends Component {
//     constructor(props) {
//         super(props);
//         this.loggedIn = false;
//     }
//     loginConfig = {
//         loginButtonText: "Login",
//         fieldsRequired: true,
//         showCancelButton: false,
//         cancelButtonText: "Cancel",
//         usernameMinLength: "3",
//         passwordMinLength: "4",
//         inputClass: "login-input",
//         buttonClass: "login-button",
//         usernameMessage: "",
//         passwordMessage: "",
//         isDisabled: true,
//         passwordForgot: true,
//         usernameForgotText: "Forgot UserID",
//         passwordForgotText: "Forgot Password",
//         usernamePlaceholder: "USER ID",
//         passwordPlaceholder: "PASSWORD",
//         usernameForgot: true,
//         inputLinkClass: "login-link",
//         isHorizontal: true
//     };

//     saveToLocalStorage = userInfo => {
//         localStorage.setItem("token", JSON.stringify(userInfo.data.user));
//         localStorage.setItem("userInfo", JSON.stringify(userInfo.data.user));
//         this.loggedIn = true;
//         this.props.onSave(userInfo);
//     };

//     render() {
//         window.addEventListener("onLogin", e => {
//             e.preventDefault();
//             const user = {
//                 username: e.detail.username,
//                 password: e.detail.password
//             };
//             this.props.sendCredentials(user);
//         });

//         if (this.props.userInfo && this.props.userInfo.data.id) {
//             this.saveToLocalStorage(this.props.userInfo);
//         }

//         return (
//             <div className="login-block">
//                 {/* {this.loggedIn === true ? (
//                     <Redirect to="/" />
//                 ) : (
//                         <Redirect to="/" />
//                     )} */}
//                 <div className="login-header">
//                     <h4>Customer Account</h4>
//                     <h4>Sign in</h4>
//                 </div>
//                 <simple-login login-config={JSON.stringify(this.loginConfig)} />
//                 <div className="signup-action">
//                     <h4>Not Registered?</h4>
//                     <h4>Sign Up</h4>
//                 </div>
//             </div>
//         );
//     }
// }

// Login.propTypes = {
//     sendCredentials: PropTypes.func,
//     userInfo: PropTypes.any,
//     onSave: PropTypes.func
// };

// const mapDispatchToPorps = dispatch => ({
//     sendCredentials: user => {
//         dispatch(checkAuth(user));
//     }
// });

// const mapStateToProps = state => ({
//     userInfo: state.auth
// });
// export default connect(mapStateToProps, mapDispatchToPorps)(Login);

import React from "react"

export default ()=> <p>Login</p>