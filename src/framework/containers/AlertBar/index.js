import React from "react";
import "./index.scss";

export default () => (
	<div className="item-gap alert-bar">
		<ul>
			{/* messages */}
			<li>
				<span className="icon-block">
					<i className="fa far fa-envelope"/>
				</span>
				<div className="info">
					<p className="title">Messages</p>
					<p className="subtitle">2 unread</p>
				</div>
				<span className="icon-block">
					<a>
						<i className="fa fa-angle-down"/>
					</a>
				</span>
				<span className="divider"></span>
			</li>

			{/* alerts */}
			<li>
				<span className="icon-block">
					<i className="fas fa-exclamation-triangle fa"/>
				</span>
				<div className="info">
					<p className="title">alerts</p>
					<p className="subtitle">2 new</p>
				</div>
				<span className="icon-block">
					<a>
						<i className="fa fa-angle-down"/>
					</a>
				</span>
				<span className="divider"></span>
				
			</li>
			{/* open requests */}
			<li>
				<span className="icon-block">
					<i className="fa fas fa-phone"/>
				</span>
				<div className="info">
					<p className="title">Open requests</p>
					<p className="subtitle">2 updates</p>
				</div>
				<span className="icon-block">
					<a>
						<i className="fa fa-angle-down"/>
					</a>
				</span>
				<span className="divider"></span>
				
			</li>
		</ul>
	</div>
);
