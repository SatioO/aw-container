export const NOT_LOADED = "NOT_LOADED";

export const toName = app => app.name;

export const defaultConfig = () =>
	new Promise((resolve, reject) => resolve({ default: {} }));
