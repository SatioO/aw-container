export default {
	layout: {
		skipHeader: new Set([]),
		skipSideBar: new Set([]),
		skipBanner: new Set([]),
		skipFooter: new Set([]),
		skipAlert: new Set([])
	}
};
