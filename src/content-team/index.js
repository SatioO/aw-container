import { Home, Profile } from "./containers";

export default [
	{
		path: "/",
		exact: true,
		component: Home
	},
	{
		path: "/myprofile",
		exact: true,
		component: Profile
	}
];
