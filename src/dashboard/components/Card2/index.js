import React from "react";
import HOC from "HOC";
import { Card } from "antd";

const gridStyle = {
	width: "25%",
	textAlign: "center"
};

const Card1 = HOC(
	props => {
		return (
			<Card title="Card title" hoverable="true">
				<p>Card content</p>
				<p>Card content</p>
				<p>Card content</p>
				<button onClick={() => props.expand({ items: [1, 2, 3] })}>
					Expand
				</button>
			</Card>
		);
	},
	props => (
		<Card>
			<Card.Grid style={gridStyle}>
				{props.params.items.map(item => <h1 key={item}>{item}</h1>)}
				<button onClick={() => props.collapse()}>Collpase</button>
			</Card.Grid>
		</Card>
	)
);

export default Card1;
