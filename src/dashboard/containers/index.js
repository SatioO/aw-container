import "./index.scss";
import React from "react";
import { Card1, Card2 } from "../components";
import { Layout } from "antd";
const { Content } = Layout;

export default ({ match }) => {
	return (
		<Layout className="cards-layout-container">
			<Content>
				<div className="columns is-multiline is-gapless cards-container is-mobile">
					<Card1 config={{ classNames: "is-8" }} />
					<Card1 />
					<Card1 />
					<Card1 />
					<Card2 config={{ classNames: "is-4" }} />
					<Card2 config={{ classNames: "is-8" }} />
					<Card1 />
					<Card2 />
					<Card1 />
				</div>
			</Content>
		</Layout>
	);
};
